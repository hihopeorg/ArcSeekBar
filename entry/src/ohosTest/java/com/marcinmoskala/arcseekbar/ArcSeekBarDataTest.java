package com.marcinmoskala.arcseekbar;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class ArcSeekBarDataTest {

    private float dx;
    private float dy;
    private float width;
    private float height;
    private int progress;
    private int maxProgress;

    @Test
    public void getDx() throws NoSuchFieldException, IllegalAccessException {
        float dx = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("dx");
        field.setAccessible(true);
        field.set(arcSeekBarData, dx);
        assertEquals(dx, arcSeekBarData.getDx(), 0);
    }

    @Test
    public void getDy() throws NoSuchFieldException, IllegalAccessException {
        float dy = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("dy");
        field.setAccessible(true);
        field.set(arcSeekBarData, dy);
        assertEquals(dy, arcSeekBarData.getDy(), 0);
    }

    @Test
    public void getWidth() throws NoSuchFieldException, IllegalAccessException {
        float width = 50;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("width");
        field.setAccessible(true);
        field.set(arcSeekBarData, width);
        assertEquals(width, arcSeekBarData.getWidth(), 0);
    }

    @Test
    public void getHeight() throws NoSuchFieldException, IllegalAccessException {
        float height = 50;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("height");
        field.setAccessible(true);
        field.set(arcSeekBarData, height);
        assertEquals(height, arcSeekBarData.getHeight(), 0);
    }

    @Test
    public void getProgress() throws NoSuchFieldException, IllegalAccessException {
        int progress = 50;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("progress");
        field.setAccessible(true);
        field.set(arcSeekBarData, progress);
        assertEquals("It's right", progress, arcSeekBarData.getProgress());
    }

    @Test
    public void getMaxProgress() throws NoSuchFieldException, IllegalAccessException {
        int maxProgress = 100;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("maxProgress");
        field.setAccessible(true);
        field.set(arcSeekBarData, maxProgress);
        assertEquals("It's right", maxProgress, arcSeekBarData.getMaxProgress());
    }

    @Test
    public void getThumbX() throws NoSuchFieldException, IllegalAccessException {
        int thumbX = 5;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("thumbX");
        field.setAccessible(true);
        field.set(arcSeekBarData, thumbX);
        assertEquals("It's right", thumbX, arcSeekBarData.getThumbX());
    }

    @Test
    public void getThumbY() throws NoSuchFieldException, IllegalAccessException {
        int thumbY = 5;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("thumbY");
        field.setAccessible(true);
        field.set(arcSeekBarData, thumbY);
        assertEquals("It's right", thumbY, arcSeekBarData.getThumbY());
    }

    @Test
    public void getStartAngle() throws NoSuchFieldException, IllegalAccessException {
        float startAngle = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("startAngle");
        field.setAccessible(true);
        field.set(arcSeekBarData, startAngle);
        assertEquals(startAngle, arcSeekBarData.getStartAngle(), 0);
    }

    @Test
    public void getSweepAngle() throws NoSuchFieldException, IllegalAccessException {
        float sweepAngle = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("sweepAngle");
        field.setAccessible(true);
        field.set(arcSeekBarData, sweepAngle);
        assertEquals(sweepAngle, arcSeekBarData.getSweepAngle(), 0);
    }

    @Test
    public void getProgressSweepRad() throws NoSuchFieldException, IllegalAccessException {
        float progressSweepRad = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("progressSweepRad");
        field.setAccessible(true);
        field.set(arcSeekBarData, progressSweepRad);
        assertEquals(progressSweepRad, arcSeekBarData.getProgressSweepRad(), 0);
    }

    @Test
    public void getProgressSweepAngle() throws NoSuchFieldException, IllegalAccessException {
        float progressSweepAngle = 1;
        ArcSeekBarData arcSeekBarData = new ArcSeekBarData(dx, dy, width, height, progress, maxProgress);
        Field field = arcSeekBarData.getClass().getDeclaredField("progressSweepAngle");
        field.setAccessible(true);
        field.set(arcSeekBarData, progressSweepAngle);
        assertEquals(progressSweepAngle, arcSeekBarData.getProgressSweepAngle(), 0);
    }
}