package com.marcinmoskala.arcseekbar;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArcSeekBarTest {

    @Test
    public void getProgress() {
        int progress = 50;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ArcSeekBar arcSeekBar = new ArcSeekBar(context, null);
        arcSeekBar.setProgress(progress);
        assertEquals("It's right", progress, arcSeekBar.getProgress());
    }

    @Test
    public void getMaxProgress() {
        int maxProgress = 100;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ArcSeekBar arcSeekBar = new ArcSeekBar(context, null);
        arcSeekBar.setMaxProgress(maxProgress);
        assertEquals("It's right", maxProgress, arcSeekBar.getMaxProgress());
    }

    @Test
    public void getOnProgressChangedListener() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ArcSeekBar arcSeekBar = new ArcSeekBar(context, null);
        arcSeekBar.setOnProgressChangedListener(new ProgressListener() {
            @Override
            public void invoke(int progress) {

            }
        });
        assertNotNull("It's right", arcSeekBar.getOnProgressChangedListener());
    }

    @Test
    public void getOnStartTrackingTouch() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ArcSeekBar arcSeekBar = new ArcSeekBar(context, null);
        arcSeekBar.setOnStartTrackingTouch(new ProgressListener() {
            @Override
            public void invoke(int progress) {

            }
        });
        assertNotNull("It's right", arcSeekBar.getOnStartTrackingTouch());
    }

    @Test
    public void getOnStopTrackingTouch() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ArcSeekBar arcSeekBar = new ArcSeekBar(context, null);
        arcSeekBar.setOnStopTrackingTouch(new ProgressListener() {
            @Override
            public void invoke(int progress) {

            }
        });
        assertNotNull("It's right", arcSeekBar.getOnStopTrackingTouch());
    }
}