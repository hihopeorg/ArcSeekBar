package com.marcinmoskala.arcseekbar;

import com.marcinmoskala.arcseekbar.util.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.DirectionalLayout;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01203, "marcinmoskalaTest");
    private static Ability ability = EventHelper.startAbility(MainAbility.class);

    public void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01First() {
        ArcSeekBar arcSeekBar = (ArcSeekBar) ability.findComponentById(ResourceTable.Id_arcseekbar);
        stopThread(1000);
        int x1 = arcSeekBar.getLocationOnScreen()[0];
        int y1 = arcSeekBar.getLocationOnScreen()[1] + arcSeekBar.getHeight() / 2;
        int x0 = arcSeekBar.getLocationOnScreen()[0] + arcSeekBar.getWidth();
        int y0 = arcSeekBar.getLocationOnScreen()[1] + arcSeekBar.getHeight() / 2;
        EventHelper.inputSwipe(ability, x0, y0, x1, y1, 5000);
        stopThread(500);
        Assert.assertTrue("进度更改失败", arcSeekBar.getProgress() == 0);
        stopThread(2000);
    }

    @Test
    public void test02() {
        ArcSeekBar arcSeekBarFirst = (ArcSeekBar) ability.findComponentById(ResourceTable.Id_arcseekbar);
        DirectionalLayout directionalLayout = (DirectionalLayout) arcSeekBarFirst.getComponentParent();
        ArcSeekBar arcSeekBar = (ArcSeekBar) directionalLayout.getComponentAt(1);
        stopThread(300);
        int x0 = arcSeekBar.getLocationOnScreen()[0];
        int y0 = arcSeekBar.getLocationOnScreen()[1] + arcSeekBar.getHeight() / 2;
        int x1 = arcSeekBar.getLocationOnScreen()[0] + arcSeekBar.getWidth();
        int y1 = arcSeekBar.getLocationOnScreen()[1] + arcSeekBar.getHeight() / 2;
        EventHelper.inputSwipe(ability, x0, y0, x1, y1, 3000);
        stopThread(2000);
        Assert.assertTrue("进度更改失败", arcSeekBar.getProgress() == arcSeekBar.getMaxProgress());
        stopThread(2000);
    }

}