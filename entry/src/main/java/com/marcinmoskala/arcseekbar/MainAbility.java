package com.marcinmoskala.arcseekbar;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

public class MainAbility extends Ability {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "MainAbility");

    private ArcSeekBar seekBar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        seekBar = (ArcSeekBar) findComponentById(ResourceTable.Id_arcseekbar);
        seekBar.setMaxProgress(200);
        seekBar.setOnProgressChangedListener(new ProgressListener() {
            @Override
            public void invoke(int progress) {
                HiLog.info(LABEL, "progress =  " + progress);
            }
        });
        try {
            Color color1 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_1).getColor());
            Color color2 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_2).getColor());
            Color color3 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_3).getColor());
            Color color4 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_4).getColor());
            Color[] colors = {color1, color2, color3, color4};
            seekBar.setProgressGradient(colors);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }
}
