# ArcSeekBar

本项目是基于开源项目ArcSeekBar进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/marcinmoskala/arcseekbar ）
追踪到原项目版本

#### 项目介绍

- 项目名称：带有弧度的seekbar
- 所属系列：ohos的第三方组件适配移植
- 功能：支持设置进度条颜色，背景色，进度条宽度，进度监听及自定义Thumb
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/marcinmoskala/arcseekbar
- 原项目基线版本：V0.34
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/ArcSeekBar.gif)

#### 安装教程

方法1. 
1. 下载har包ArcSeekBar.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ArcSeekBar', ext: 'har')
	……
}
```
方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.github.marcinmoskala.ohos:ArcSeekBar:1.0.1'
}
```

#### 使用说明

1. 布局文件定义，下面是一些使用范例：
```xml
<com.marcinmoskala.arcseekbar.ArcSeekBar
    ohos:width="300vp"
    ohos:height="150vp"/>
```
 同样可以设置自定义的thumb：
```xml
<com.marcinmoskala.arcseekbar.ArcSeekBar
    ohos:width="200vp"
    ohos:height="100vp"
    ohos:margin="20vp"
    ohos:padding="20vp" 
    app:thumb="$graphic:thumb2"
    app:progressColor="#FF4081"
    app:progressBackgroundColor="#3F51B5"/>
```
 也可以设置进度条宽度，进度等：
```xml
<com.marcinmoskala.arcseekbar.ArcSeekBar
    ohos:id="$+id:arcseekbar"
    ohos:height="100vp"
    ohos:width="match_parent"
    ohos:padding="20vp"
    app:progressBackgroundColor="#4227395a"
    app:progressBackgroundWidth="20vp"
    app:progressWidth="20vp"
    app:progress="100"
    app:roundEdges="true" />
```
2. 如果想要使用颜色渐变效果，你可以在代码中这样写：
```java
Color color1 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_1).getColor());
Color color2 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_2).getColor());
Color color3 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_3).getColor());
Color color4 = new Color(this.getResourceManager().getElement(ResourceTable.Color_color_4).getColor());
Color[] colors = {color1, color2, color3, color4};
seekBar.setProgressGradient(colors);
```
3. 下面是自定义属性的详细介绍：
* `thumb` - 自定义的thumb Element
* `progress` - 初始化进度值 (0 by default)
* `maxProgress` - 最大进度值 (100 by default)
* `progressColor` - 进度条颜色
* `progressWidth` - 进度条宽度
* `progressBackgroundColor` - 进度条背景色
* `progressBackgroundWidth` - 进度条背景的宽度
* `roundEdges` - 是否为圆角 (`true` by default).


#### 版本迭代

- v1.0.1

#### 版权和许可信息

- Apache Licence.
