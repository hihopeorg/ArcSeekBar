package com.marcinmoskala.arcseekbar;

import ohos.agp.utils.RectFloat;

public class ArcSeekBarData {

    private final float pi;
    private final float zero;
    private final float r;
    private final float circleCenterX;
    private final float circleCenterY;
    private final float alphaRad;
    private final RectFloat arcRect;
    private final float startAngle;
    private final float sweepAngle;
    private final float progressSweepRad;
    private final float progressSweepAngle;
    private final int thumbX;
    private final int thumbY;

    private float dx;
    private float dy;
    private float width;
    private float height;
    private int progress;
    private int maxProgress;

    public ArcSeekBarData(float dx, float dy, float width, float height, int progress, int maxProgress) {
        this.dx = dx;
        this.dy = dy;
        this.width = width;
        this.height = height;
        this.progress = progress;
        this.maxProgress = maxProgress;

        this.pi = (float) Math.PI;
        this.zero = 0.0001F;
        this.r = this.height / 2 + this.width * this.width / 8 / this.height;
        this.circleCenterX = width / 2 + this.dy;
        this.circleCenterY = r + this.dx;
        this.alphaRad = Util.bound(zero, (float) Math.acos((double) (r - this.height) / r), 2 * pi);
        this.arcRect = new RectFloat(circleCenterX - r, circleCenterY - r, circleCenterX + r, circleCenterY + r);
        this.startAngle = Util.bound(180F, 270 - alphaRad / 2 / pi * 360F, 360F);
        this.sweepAngle = Util.bound(zero, (2F * alphaRad) / 2 / pi * 360F, 180F);
        this.progressSweepRad = this.maxProgress == 0 ? zero : Util.bound(zero, (float) this.progress / this.maxProgress * 2 * alphaRad, 2 * pi);
        this.progressSweepAngle = progressSweepRad / 2 / pi * 360F;
        this.thumbX = (int) (r * (float) Math.cos(alphaRad + pi / 2 - progressSweepRad) + circleCenterX);
        this.thumbY = (int) (-r * (float) Math.sin(alphaRad + pi / 2 - progressSweepRad) + circleCenterY);
    }

    Integer progressFromClick(float x, float y, int thumbHeight) {
        if (y > height + dy * 2) {
            return null;
        }
        final double distToCircleCenter = Math.sqrt(Math.pow(circleCenterX - (double) x, 2.0) + Math.pow(circleCenterY - (double) y, 2.0));
        if (Math.abs(distToCircleCenter - r) > thumbHeight) {
            return null;
        }
        final float innerWidthHalf = width / 2;
        final double xFromCenter = Util.bound(-innerWidthHalf, x - circleCenterX, innerWidthHalf).doubleValue();
        final double touchAngle = Math.acos(xFromCenter / r) + alphaRad - Math.PI / 2;
        final double angleToMax = 1.0 - touchAngle / (2 * alphaRad);
        int progress = Util.bound(0, (int) ((maxProgress + 1) * angleToMax), maxProgress);
        return progress;
    }

    public float getDx() {
        return this.dx;
    }

    public float getDy() {
        return this.dy;
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public int getProgress() {
        return this.progress;
    }

    public int getMaxProgress() {
        return this.maxProgress;
    }

    public int getThumbX() {
        return this.thumbX;
    }

    public int getThumbY() {
        return this.thumbY;
    }

    public final RectFloat getArcRect() {
        return this.arcRect;
    }

    public final float getStartAngle() {
        return this.startAngle;
    }

    public final float getSweepAngle() {
        return this.sweepAngle;
    }

    public final float getProgressSweepRad() {
        return this.progressSweepRad;
    }

    public final float getProgressSweepAngle() {
        return this.progressSweepAngle;
    }
}
