package com.marcinmoskala.arcseekbar;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class ArcSeekBar extends Component implements Component.DrawTask, Component.TouchEventListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ArcSeekBar");

    private ProgressListener onProgressChangedListener;
    private ProgressListener onStartTrackingTouch;
    private ProgressListener onStopTrackingTouch;

    private int maxProgress;
    private int progress;
    private float progressWidth;
    private float progressBackgroundWidth;
    private Paint progressBackgroundPaint;
    private Paint progressPaint;
    private boolean roundedEdges;
    private Color progressBackgroundColor;
    private Color progressColor;
    private boolean mEnabled;
    private ArcSeekBarData drawData;
    private Element element;
    private Color[] gradientColors;

    public ArcSeekBar(Context context) {
        super(context);
    }

    public ArcSeekBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(attrSet);
        element = Util.getElement(context, attrSet, "thumb");
    }

    private void initView(AttrSet attrSet) {
        maxProgress = Util.getIntegerValue(attrSet, "maxProgress", 100);
        progress = Util.getIntegerValue(attrSet, "progress", 0);
        progressWidth = Util.getDimensionValue(attrSet, "progressWidth", 6);
        progressBackgroundWidth = Util.getDimensionValue(attrSet, "progressBackgroundWidth", 6);
        progressColor = Util.getColorValue(attrSet, "progressColor", "#ff33b5e5");
        progressBackgroundColor = Util.getColorValue(attrSet, "progressBackgroundColor", "#4227395a");
        roundedEdges = Util.getBoolValue(attrSet, "roundedEdges", true);
        mEnabled = Util.getBoolValue(attrSet, "mEnabled", true);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        float width = component.getWidth();
        float height = component.getHeight();
        progressBackgroundPaint = makeProgressPaint(progressBackgroundColor, progressBackgroundWidth);
        progressPaint = makeProgressPaint(progressColor, progressWidth);

        measureComponent(width, height);

        Arc sArc = new Arc(drawData.getStartAngle(), drawData.getSweepAngle(), false);
        canvas.drawArc(drawData.getArcRect(), sArc, progressBackgroundPaint);

        Arc pArc = new Arc(drawData.getStartAngle(), drawData.getProgressSweepAngle(), false);
        canvas.drawArc(drawData.getArcRect(), pArc, progressPaint);

        if (mEnabled) {
            drawThumb(canvas);
        }
    }

    private void drawThumb(Canvas canvas) {
        int thumbHalfHeight = 30;
        int thumbHalfWidth = 30;
        element.setBounds(drawData.getThumbX() - thumbHalfWidth, drawData.getThumbY() - thumbHalfHeight,
                drawData.getThumbX() + thumbHalfWidth, drawData.getThumbY() + thumbHalfHeight);
        element.drawToCanvas(canvas);
    }

    private void measureComponent(float width, float height) {
        float dx = Math.max(30, progressWidth) + 6;
        float dy = Math.max(30, progressWidth) + 6;
        float realWidth = width - 2 * dx - getPaddingLeft() - getPaddingRight();
        float realHeight = Math.min(height - 2 * dy - getPaddingTop() - getPaddingBottom(), realWidth / 2);
        drawData = new ArcSeekBarData(dx + getPaddingLeft(), dy + getPaddingTop(), realWidth, realHeight, progress, maxProgress);

        if (null != gradientColors) {
            setShader(progressPaint, gradientColors);
        }
    }

    private Paint makeProgressPaint(Color color, float width) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(width);
        if (this.roundedEdges) {
            paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        }
        return paint;
    }

    private void updateOnTouch(Component component, TouchEvent touchEvent) {
        if (drawData != null) {
            int[] location = component.getLocationOnScreen();
            MmiPoint point = touchEvent.getPointerScreenPosition(touchEvent.getIndex());
            float x = point.getX() - location[0];
            float y = point.getY() - location[1];
            Integer progress = drawData.progressFromClick(x, y, 60);
            if (progress != null) {
                int progressFromClick = progress;
                setPressState(true);
                setProgress(progressFromClick);
                invalidate();
                return;
            }
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (mEnabled) {
            ProgressListener listener;
            switch (touchEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    listener = this.onStartTrackingTouch;
                    if (listener != null) {
                        listener.invoke(progress);
                    }
                    updateOnTouch(component, touchEvent);
                    break;
                case TouchEvent.POINT_MOVE:
                    updateOnTouch(component, touchEvent);
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                case TouchEvent.CANCEL:
                    listener = this.onStopTrackingTouch;
                    if (listener != null) {
                        listener.invoke(progress);
                    }
                    setPressState(false);
                    break;
            }
        }
        return mEnabled;
    }

    public void setProgressGradient(Color... colors) {
        setGradient(progressPaint, colors);
    }

    private void setGradient(Paint paint, Color... colors) {
        if (drawData != null) {
            setShader(paint, colors);
        } else {
            gradientColors = colors;
        }
    }

    private void setShader(Paint paint, Color... colors) {
        Point startPoint = new Point(drawData.getDx(), 0);
        Point endPoint = new Point(drawData.getWidth(), 0);
        Point[] points = {startPoint, endPoint};
        Shader shader = new LinearShader(points, null, colors, Shader.TileMode.CLAMP_TILEMODE);
        paint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
        invalidate();
    }

    public int getProgress() {
        return this.progress;
    }

    public void setProgress(int progress) {
        this.progress = Util.bound(0, progress, this.maxProgress);
        ProgressListener listener = this.onProgressChangedListener;
        if (listener != null) {
            listener.invoke(progress);
        }
        invalidate();
    }

    public int getMaxProgress() {
        return this.maxProgress;
    }

    public void setMaxProgress(int progress) {
        this.maxProgress = Util.bound(0, progress, Integer.MAX_VALUE);
        invalidate();
    }

    public Color getProgressColor() {
        return this.progressColor;
    }

    public void setProgressColor(Color color) {
        progressPaint.setColor(color);
        invalidate();
    }

    public Color getProgressBackgroundColor() {
        return this.progressBackgroundColor;
    }

    public void setProgressBackgroundColor(Color color) {
        progressBackgroundPaint.setColor(color);
        invalidate();
    }

    public void setRoundedEdges(boolean roundedEdges) {
        if (roundedEdges) {
            progressBackgroundPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
            progressPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        } else {
            progressBackgroundPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
            progressPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        }

        this.roundedEdges = roundedEdges;
    }

    public ProgressListener getOnProgressChangedListener() {
        return this.onProgressChangedListener;
    }

    public void setOnProgressChangedListener(ProgressListener listener) {
        this.onProgressChangedListener = listener;
    }

    public ProgressListener getOnStartTrackingTouch() {
        return this.onStartTrackingTouch;
    }

    public void setOnStartTrackingTouch(ProgressListener listener) {
        this.onStartTrackingTouch = listener;
    }

    public ProgressListener getOnStopTrackingTouch() {
        return this.onStopTrackingTouch;
    }

    public void setOnStopTrackingTouch(ProgressListener listener) {
        this.onStopTrackingTouch = listener;
    }
}
