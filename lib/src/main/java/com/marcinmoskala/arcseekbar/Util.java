package com.marcinmoskala.arcseekbar;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

public class Util {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Util");

    public static <T extends Number> T bound(T min, T value, T max) {
        if (value.doubleValue() > max.doubleValue()) {
            return max;
        } else if (value.doubleValue() < min.doubleValue()) {
            return min;
        } else {
            return value;
        }
    }

    public static int getIntegerValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getIntegerValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static boolean getBoolValue(AttrSet attrSet, String name, boolean defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            boolean value = attrSet.getAttr(name).get().getBoolValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static Color getColorValue(AttrSet attrSet, String name, String defaultValue) {
        if (attrSet == null) {
            return new Color(0);
        }
        try {
            String value = attrSet.getAttr(name).get().getStringValue();
            Color color = new Color(Color.getIntColor(value));
            return color;
        } catch (NoSuchElementException e) {
            Color defaultColor = new Color(Color.getIntColor(defaultValue));
            return defaultColor;
        }
    }

    public static int getDimensionValue(AttrSet attrSet, String name, int defaultValue) {
        if (attrSet == null) {
            return defaultValue;
        }
        try {
            int value = attrSet.getAttr(name).get().getDimensionValue();
            return value;
        } catch (NoSuchElementException e) {
            return defaultValue;
        }
    }

    public static Element getElement(Context context, AttrSet attrSet, String name) {
        if (attrSet == null) {
            return null;
        }
        Element element;
        try {
            element = attrSet.getAttr(name).get().getElement();
        } catch (Exception e) {
            element = new ShapeElement(context, ResourceTable.Graphic_thumb);
        }
        return element;
    }
}
